#!/bin/bash
set -e

export PATH="/home/ubuntu/.nvm/versions/node/v18.16.1/bin:$PATH"

echo "--- Pulling updated code --- "
cd /home/ubuntu/pizzeria
git checkout main
git pull origin main

echo "--- Building backend --- "
npm install
npm install -g pm2
pm2 restart index.js --name "pizzeria-server"

echo "--- Building frontend --- "
cd /home/ubuntu/pizzeria/public
npm install
npm run build

echo "--- Copying build to nginx --- "
sudo cp -R /home/ubuntu/pizzeria/public/dist/* /var/www/pizzeria

